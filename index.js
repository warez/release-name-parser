const regexes = {
    date: /^(([0-9]{4}( [0-9]{2}){2})|(([0-9]{2} ){2}[0-9]{4})|([0-9]{2} [0-9]{2} [0-9]{2}))$/,
    years: /^[0-9]{4}$/,
    format: /^(flac|mp[e34]g?|m4a|m[4k]v|iso|avi)$/i,
    bitrate: /^[0-9]+( [0-9]+)?k?b(it)?[\/p]s$/i,
    bitDepth: /^[0-9]+ ?b(its?)?$/i,
    sampleRate: /^[0-9]+( [0-9]+)?k?hz$/i,
    languages: /^(multi|(true|sub)? ?fr((e(nch)|a)?)?|eng(lish)?|german|hindi|italian|spanish|vo?(st)?(fr?[2fqi]?|en|a)?)$/i,
    resolution: /^[0-9]{3,4}[pi]$/i,
    source: /^((blu ?ray|b[dr]|cd|dvd|hd|tv|hdtv|web|nf|amzn?|dsnp|atvp|pmtp) ?(r(ip)?|dl)?)$/i,
    codecs: /^([hx] ?26[45]|(a|he)vc|aac|vc ?1|xvid|mpeg ?[24])$/i,
    dolbySpecs: /^(dolby )?(true ?hd|atmos|(e ?)?ac ?3|ddp?([257] ?[01])?)$/i,
    isDolbyVision: /^d(o(lby)?)? ?v(i(sion)?)?$/i,
    dtsSpecs: /^(dts|(dts ?)?hd ?ma)$/i,
    isLight: /^((hd|4k)light|mhd)$/i,
    surround: /^[1257] [01]$/,
    episode: /^s?[0-9]{2,}[ex][0-9]{2,}$/i,
    seasonNumber: /^s((ea|ai)son)? ?[0-9]+$/i,
    episodeNumber: /^e(pisode)? ?[0-9]+$/i,
    isRemux: /^(remux|full)$/i,
    isHdr: /^hdr(10)?$/i,
    ignored: /^((u(ltra)?|t)?hd|pal|ntsc|sdr|dvd ?[59]|internal|crf|proper|dubb|[0-9]+[ x]?([mg]b|cd|dvd|dis[ck]s|ep)[0-9]?|x{3}|compl[eè]te|custom|hybrid|v[0-9]|final|remaster(ed)?|pcm|video ts|repack|unrated|dxva|ld|((extended)?(unrated)?(director'?s)? ?(cut)?)|unrated dc|int[eé]grale?|fastsub|[48]k|he|hires|restored)$/i
};

export default releaseName => {
    const
        chunks = releaseName
            .split(/([\s\-._()&+\[\],:|]+)/g)
            .filter(Boolean)
            .reduce((chunks, value, index) => [
                ...chunks,
                { value, isSeparator: index % 2 === 1 }
            ], []),
        altTitleStartChunkIndex = releaseName.endsWith(')') && chunks.findIndex(chunk => chunk.value === ' ('),
        result = {
            releaseName,
            chunks,
            title: '',
            altTitle: altTitleStartChunkIndex > 0
                ? chunks.slice(altTitleStartChunkIndex + 1, -1).map(chunk => {
                    chunk.property = 'altTitle';
                    return chunk.value;
                }).join('')
                : undefined
        },
        dataChunks = chunks.filter(chunk => !chunk.isSeparator && !chunk.property);
    let titleIndex = 0;
    for(let startChunkIndex = 0; startChunkIndex < dataChunks.length; startChunkIndex++){
        let property;
        for(let endChunkIndex = dataChunks.length; endChunkIndex > startChunkIndex; endChunkIndex--){
            if(!result.title) continue;
            ([property] = Object.entries(regexes).find(([, regex]) => regex.test(dataChunks.slice(startChunkIndex, endChunkIndex).map(_ => _.value).join(' '))) || []);
            if(!property) continue;
            const
                selectedChunks = chunks.slice(chunks.indexOf(dataChunks[startChunkIndex]), chunks.indexOf(dataChunks[endChunkIndex]) - 1),
                selectedChunksString = selectedChunks.map(_ => _.value).join('');
            selectedChunks.forEach(chunk => chunk.property = property);
            if(property !== 'ignored'){
                if(property.startsWith('is'))
                    result[property] = true;
                else if(property.endsWith('s')){
                    if(!result[property])
                        result[property] = [];
                    if(!result[property].includes(selectedChunksString))
                        result[property].push(selectedChunksString);
                }
                else result[property] = selectedChunksString;
            }
            startChunkIndex = endChunkIndex - 1;
            break;
        }
        if((!property || property === 'ignored') && startChunkIndex === titleIndex++){
            dataChunks[startChunkIndex].property = 'title';
            result.title += `${result.title ? ' ' : ''}${dataChunks[startChunkIndex].value}`;
        }
    }
    for(let chunkIndex = dataChunks.length - 1; chunkIndex > 0; chunkIndex--){
        const chunk = dataChunks[chunkIndex];
        if(chunk.property) break;
        chunk.property = 'tag';
        result.tag = `${chunk.value}${result.tag ? ` ${result.tag}` : ''}`;
    }
    const unknown = dataChunks.filter(chunk => !chunk.property).map(chunk => chunk.value);
    if(unknown.length)
        result.unknown = unknown;
    return result;
};